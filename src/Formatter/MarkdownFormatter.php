<?php

declare(strict_types=1);

namespace MondayFactory\ReporterSenderGitlab\Formatter;

use MondayFactory\Reporter\Message\ArrayMessageBody;
use MondayFactory\Reporter\Message\MessagePart;
use MondayFactory\Reporter\Message\MessagePartCollection;
use MondayFactory\Reporter\Message\StringMessageBody;

class MarkdownFormatter
{

	/**
	 * @param string $description
	 * @param MessagePartCollection $messagePartCollection
	 *
	 * @return string
	 */
	public static function formatMessage(string $description, MessagePartCollection $messagePartCollection): string
	{
		$message = [];

		$message[] = $description;

		if ($messagePartCollection->count() > 0) {
			$message[] = "\n---";
			$message[] = "## Additional Info";

			/**
			 * @var MessagePart $part
			 */
			foreach ($messagePartCollection->getParts() as $part) {
				$body = '';

				if ($part->getBody() instanceof ArrayMessageBody) {
					$body = self::formatAsTable($part->getBody());
				} elseif ($part->getBody() instanceof StringMessageBody) {
					$body = self::formatAsString($part->getBody());
				}

				if (is_null($part->getLink())) {
					$message[] = '### ' . $part->getTitle() . "\n";
				} else {
					$message[] = '### [' . $part->getTitle() . "](" . $part->getLink() . ")\n";
				}

				$message[] = $body;
			}
		}

		return implode("\n", $message);
	}

	/**
	 * @param ArrayMessageBody $body
	 *
	 * @return string
	 */
	private static function formatAsTable(ArrayMessageBody $body)
	{
		if (count($body->getBody()) > 0) {
			$result = "| Key | Value |\n";
			$result .= "| --- | --- |\n";

			foreach ($body->getBody() as $name => $value) {
				$result .= sprintf("| %s | %s |\n", $name, $value);
			}

			return $result . "\n";
		}

		return '';
	}

	/**
	 * @param StringMessageBody $body
	 *
	 * @return string
	 */
	private static function formatAsString(StringMessageBody $body)
	{
		return strlen($body->getBody()) > 0 ? "```" . $body->getBody() . "```" : "-\n";
	}

	/**
	 * @param array $attachments
	 *
	 * @return string
	 */
	public static function formatAttachments(array $attachments): string
	{
		$result = "## Attachments\n\n";

		foreach ($attachments as $attachment) {
			$result .= '![' . $attachment['name'] . '](' . $attachment['url'] . ")\n";
		}

		return $result;
	}
}
