<?php

declare(strict_types=1);

namespace MondayFactory\ReporterSenderGitlab\Sender;

use Gitlab\Client;
use Gitlab\Model\Project;
use MondayFactory\Reporter\Message\MessagePart;
use MondayFactory\Reporter\Message\MessagePartCollection;
use MondayFactory\Reporter\Sender\AbstractSender;
use MondayFactory\Reporter\Sender\ISender;
use MondayFactory\ReporterSenderGitlab\Formatter\MarkdownFormatter;
use Nette\Application\UI\Form;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;
use Nette\Utils\Finder;
use Nette\Utils\Strings;

class GitlabSender extends AbstractSender implements ISender
{

	/**
	 * @var array
	 */
	protected $config;

	/**
	 * @var array
	 */
	protected $minimalConfig = [
		'url' => null,
		'token' => null,
		'username' => null,
		'projectId' => null,
	];

	/**
	 * @var Client
	 */
	private $gitlabClient;

	/**
	 * @var MessagePartCollection
	 */
	private $messagePartCollection;

	/**
	 * @param MessagePartCollection $messagePartCollection
	 */
	public function __construct(MessagePartCollection $messagePartCollection)
	{
		$this->messagePartCollection = $messagePartCollection;
	}

	/**
	 * @param Form $form
	 * @param ArrayHash $values
	 */
	public function send(Form $form, ArrayHash $values): void
	{
		$values = $form->getValues();
		$description = MarkdownFormatter::formatMessage($values['message'], $this->messagePartCollection);

		$uploadedAttachments = [];

		/**
		 * @var FileUpload $attachment
		 */
		foreach ($values['attachments'] as $attachment) {
			if ($attachment->isOk()) {
				$uploadedFile = $this->getGitlabClient()->projects()->uploadFile($this->config['projectId'], $attachment->getTemporaryFile());
				$uploadedAttachments[] = [
					'name' => $attachment->getName(),
					'url' => $uploadedFile['url'],
				];
			}
		}

		if (count($uploadedAttachments) > 0) {
			$description .= "\n\n" . MarkdownFormatter::formatAttachments($uploadedAttachments);
		}

		$this->getProject()->createIssue($values['subject'],
			[
				'description' => $description,
			]
		);
	}

	/**
	 * @return Client
	 */
	private function getGitlabClient(): Client
	{
		if (! $this->gitlabClient instanceof Client) {
			return  Client::create($this->config['url'])
				->authenticate($this->config['token'], Client::AUTH_HTTP_TOKEN);
		}

		return $this->gitlabClient;
	}

	/**
	 * @return Project
	 */
	private function getProject(): Project
	{
		return new Project($this->config['projectId'], $this->getGitlabClient());
	}

}
