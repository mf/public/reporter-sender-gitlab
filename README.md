## Minimal configuration

```yaml

```

```yaml
reporter:
	senders:
		gitlab:
			class: MondayFactory\ReporterSenderGitlab\Sender\GitlabSender
			config:
				url: https://gitlab.mondayfactory.cz
				token: ***
				username: johndoe
				projectId: 1
```